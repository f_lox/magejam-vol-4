﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossroad_CarsMovement : MonoBehaviour
{
    private Rigidbody2D _thisRb;
    [SerializeField] private Vector2 _velocity;

    private void Awake()
    {
        _thisRb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        _thisRb.MovePosition(_thisRb.position + _velocity * Time.deltaTime);
    }
}
