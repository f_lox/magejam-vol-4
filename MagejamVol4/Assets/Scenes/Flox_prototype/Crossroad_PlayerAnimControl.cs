﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossroad_PlayerAnimControl : MonoBehaviour
{
    private Animator _playerAnimator;
    private Rigidbody2D _playerRigidbody;

    private void Awake()
    {
        _playerRigidbody = GetComponent<Rigidbody2D>();
        _playerAnimator = GetComponent<Animator>();
    }

    private void DirectionSwitch(bool isLeft, bool isRight, bool isUp, bool isDown)
    {
        _playerAnimator.SetBool("isWalkingRight", isRight);
        _playerAnimator.SetBool("isWalkingLeft", isLeft);
        _playerAnimator.SetBool("isWalkingUp", isUp);
        _playerAnimator.SetBool("isWalkingDown", isDown);
    }

    private void Update()
    {
        if (_playerRigidbody.velocity.x > 0)
        {
            DirectionSwitch(false, true, false, false);
        }
        if (_playerRigidbody.velocity.x < 0)
        {
            DirectionSwitch(true, false, false, false);
        }
        if (_playerRigidbody.velocity.y > 0)
        {
            DirectionSwitch(false, false, true, false);
        }
        if (_playerRigidbody.velocity.y < 0)
        {
            DirectionSwitch(false, false, false, true);
        }
        if (_playerRigidbody.velocity.x == 0 && _playerRigidbody.velocity.y == 0)
        {
            DirectionSwitch(false, false, false, false);
        }
    }
}
