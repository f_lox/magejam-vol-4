﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Crossroad_PlayerDies : MonoBehaviour
{
    private Vector2 _startingPosition;

    private void Awake()
    {
        _startingPosition = transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Crossroad_CarsMovement>())
        {
            transform.localPosition = new Vector3(-0.4f, -5.08f, 16.15068f);
        }
    }
}
