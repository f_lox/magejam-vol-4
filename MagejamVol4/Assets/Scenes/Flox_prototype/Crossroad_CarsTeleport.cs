﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossroad_CarsTeleport : MonoBehaviour
{
    [SerializeField] private Vector2 _respawnCoordinates;
    [SerializeField] private Transform _respawnPos;

    private void OnTriggerEnter2D(Collider2D car)
    {
        if (car.gameObject.GetComponent<Crossroad_TeleportDetect>())
        {
            car.gameObject.transform.parent.position = _respawnPos.position;
        }
    }
}
