﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossroad_Finish : MonoBehaviour
{
    [SerializeField] private GameObject _whiteNoise;
    [SerializeField] private Collider2D _playerCollider;

    private void Awake()
    {
        _whiteNoise.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //zmiana poziomu - tymczas
        if (collision == _playerCollider)
        {
            StartCoroutine(NoiseManager.noiseManager.WaitForNoise());
        }
    }
}
