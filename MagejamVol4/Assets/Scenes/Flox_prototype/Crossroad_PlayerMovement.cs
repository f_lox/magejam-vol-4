﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossroad_PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    private Rigidbody2D _rb;
    private Vector2 _movement;

    private void MoveCharacter(Vector2 direction)
    {
        _rb.velocity = direction * _speed * Time.fixedDeltaTime;
    }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void FixedUpdate()
    {
        MoveCharacter(_movement);
    }
}
