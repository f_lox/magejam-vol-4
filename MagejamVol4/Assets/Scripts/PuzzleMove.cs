﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleMove : MonoBehaviour
{
    public GameObject socket;
    public float snapDistance;
    public bool grabbed;
    public bool inSocket = false;
    public GameObject canvasDone;
    private RaycastHit hit;
    [SerializeField] private Camera _camera;

    private void Update()
    {
        DragOnClick();
        if (grabbed)
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                transform.localPosition = new Vector3(hit.point.x, hit.point.y, transform.localPosition.z);
            }

            SnapToSocket();
        }

    }

    private void SnapToSocket()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            grabbed = false;
            if (Vector3.Distance(socket.transform.localPosition, this.transform.localPosition) < snapDistance)
            {
                this.transform.localPosition = socket.transform.localPosition;
                GetComponent<BoxCollider>().enabled = false;
                inSocket = true;
                this.gameObject.transform.SetParent(canvasDone.transform);
                PuzzleManager.puzzleManager.PuzzleCompletionCheck();
            }
        }
    }

    private void DragOnClick()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                //Debug.Log(hit.distance);
                if (hit.collider.gameObject == this.gameObject)
                {
                    grabbed = true;
                    //Debug.Log(hit.point);
                }

            }
        }
    }
}
