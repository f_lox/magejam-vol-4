﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{
    [SerializeField] private AudioSource _cubeCollectSound;
    [SerializeField] private int _maxOfCollectibles = 3;
    private int _amountOfCollectibles = 0;
    private string _keyTagName = "Key";
    private string _collectibleTagName = "Collectible";
    // script to pickup key
    public bool haveKey;
    public bool collectedCubes;
    private void OnTriggerEnter2D(Collider2D Collision)
    {
        Debug.Log("You've entered an trigger");
        if (Collision.gameObject.tag ==_keyTagName)
        {
            Debug.Log("You've entered on a key");
            Collision.gameObject.SetActive(false);
            haveKey = true;
        }
        else if(Collision.gameObject.tag == _collectibleTagName)
        {
            _cubeCollectSound.Play();
            Debug.Log("You've entered an collectible!");
            Collision.gameObject.SetActive(false);
            _amountOfCollectibles++;
        }
    }
    private void CollectorChecker()
    {
        if (_amountOfCollectibles == _maxOfCollectibles)
        {
            collectedCubes = true;
        }
    }
    private void Update()
    {
        CollectorChecker();
    }
}
