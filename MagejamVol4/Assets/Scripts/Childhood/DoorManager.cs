﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DoorManager : MonoBehaviour
{
    [SerializeField] private PickupManager _pickupManager;

    private AudioSource _lockedDoorSoundEffect;

    [SerializeField] private GameObject _noiseEffect;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (_pickupManager.haveKey == true && _pickupManager.collectedCubes == true)
        {
            // door can open
            // text will show up - press e to open doors
            // gameObject.SetActive(false); // disabling doors for testing
            // open the doors go to the next level?
            StartCoroutine(NoiseManager.noiseManager.WaitForNoise());
            
        }
        else
        {
            _lockedDoorSoundEffect.Play();
        }
    }
    private void Awake()
    {
        _pickupManager.GetComponent<PickupManager>();
        _lockedDoorSoundEffect = GetComponent<AudioSource>();
    }
}