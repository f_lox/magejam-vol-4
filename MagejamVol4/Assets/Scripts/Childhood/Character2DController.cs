﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2DController : MonoBehaviour
{
    [SerializeField] private Transform _groundChecker;
    [SerializeField] private LayerMask _groundMask;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    
    private Rigidbody2D _rb;
    private Animator _myAnim;
    private string _horizontalAxisName = "Horizontal";

    private float _zeroVal = 0;
    private float _oneHundredEightyVal = 180f;
    private float _horizontalMovement;
    private bool _isGrounded;
    private float _groundCheckRatio = 0.2f;
    private string _moveParameterBoolName = "Move";
    private void GroundCheck()
    {
        _isGrounded = Physics2D.OverlapCircle(_groundChecker.position, _groundCheckRatio, _groundMask);
    }
    private void AnimationChanger()
    {
        if(_horizontalMovement!= _zeroVal)
        {
            _myAnim.SetBool(_moveParameterBoolName, true);
        }
        else
        {
            _myAnim.SetBool(_moveParameterBoolName, false);
        }
    }
    private void CharacterRotation()
    {
        if(!Mathf.Approximately(_zeroVal, _horizontalMovement))
        {
            transform.rotation = _horizontalMovement < _zeroVal ? Quaternion.Euler(_zeroVal, _oneHundredEightyVal, _zeroVal) : Quaternion.identity;
        }
    }
    private void Movement()
    {
        _horizontalMovement = Input.GetAxis(_horizontalAxisName);
        transform.position += new Vector3(_horizontalMovement, _zeroVal, _zeroVal) * _speed * Time.deltaTime;
        CharacterRotation();
        if (Input.GetKeyDown(KeyCode.Space)&&_isGrounded == true)
        {
            _rb.AddForce(new Vector2(_zeroVal, _jumpForce), ForceMode2D.Impulse);
        }
    }
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _myAnim = GetComponent<Animator>();
    }
    private void Update()
    {
        GroundCheck();
        AnimationChanger();
        Movement();
    }
}
