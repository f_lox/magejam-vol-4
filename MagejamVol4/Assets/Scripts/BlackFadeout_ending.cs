﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BlackFadeout_ending : MonoBehaviour
{
    private Image _fadeImage;
    private int _fadeToBlackTime = 2;
    private int _textFadeIn;
    private bool _isFadingFinished;
    private bool _isTextAnimRunning;
    [SerializeField] private Animator _endText;
    [SerializeField] private GameObject _exitButton;

    private void Awake()
    {
        _fadeImage = GetComponent<Image>();
        _isFadingFinished = false;
        _isTextAnimRunning = false;
        _exitButton.SetActive(false);
    }

    public void StartEndingFade()
    {
        StartCoroutine(FadeAwayBlack());
    }

    private IEnumerator FadeAwayBlack()
    {
        for (float i = 0; i <= _fadeToBlackTime; i += Time.deltaTime)
        {
            // set color with i as alpha
            _fadeImage.color = new Color(0, 0, 0, i);
            yield return null;
        }
        _isFadingFinished = true;
    }

    private void Update()
    {
        if (_isFadingFinished && !_isTextAnimRunning)
        {
            _endText.SetTrigger("isText");
            _isTextAnimRunning = true;
            //dodać tutaj guzik do exit czy cos
            _exitButton.SetActive(true);
        }
    }
}
