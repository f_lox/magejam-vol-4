﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSocket : MonoBehaviour
{
    public GameObject puzzle;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision");
        if (other.gameObject == puzzle)
        {
            Debug.Log("Collision with puzzle");

            puzzle.transform.localPosition = this.transform.localPosition;
            puzzle.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
