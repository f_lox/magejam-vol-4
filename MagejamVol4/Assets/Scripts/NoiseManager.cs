﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _gamesArray;
    [SerializeField] private GameObject[] _noisesArray;

    private int _noiseCount = 0;

    public static NoiseManager noiseManager;

    private void Start()
    {
        noiseManager = this;
    }

    public IEnumerator WaitForNoise()
    {
        yield return new WaitForSeconds(0.5f);
        _noisesArray[_noiseCount].SetActive(true);
        yield return new WaitForSeconds(0.5f);
        _noisesArray[_noiseCount].SetActive(false);
        _noiseCount++;
        _gamesArray[_noiseCount].SetActive(true);
        _gamesArray[_noiseCount - 1].SetActive(false);
    }
}
