﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform _groundChecker;
    [SerializeField] private LayerMask _groundMask;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;

    private Rigidbody2D _rb;
    private Vector2 _jumpDirection;
    private Vector2 _movementDirection;

    private float _horizontalDirection;
    private float _oneVal = 1;
    private float _zeroVal = 0;
    private float _negativeTwoVal = -2;
    private float _negativeElevenVal = -11;

    private bool _isGrounded;
    private string _horizontalAxis = "Horizontal";
    private string _groundTagName = "Ground";

    private float _groundDistance = 0.2f;
    private Vector3 _velocity;

    private void Movement()
    {
        _horizontalDirection = Input.GetAxis(_horizontalAxis);
        _movementDirection = new Vector2(_horizontalDirection, _zeroVal);
        _rb.MovePosition((Vector2)transform.position + (_movementDirection * _speed * Time.deltaTime));
        _jumpDirection = new Vector2(_zeroVal, _jumpForce);
    }
    private void GroundCheck()
    {
        _isGrounded = Physics2D.OverlapCircle(_groundChecker.position, _groundDistance, _groundMask);
        /*
        if(_isGrounded && _velocity.y < _negativeElevenVal)
        {
            _velocity.y = _negativeTwoVal;
        }
        */
        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded == true)
        {
            _velocity.y = Mathf.Sqrt(_jumpForce * _negativeTwoVal * _jumpForce);
     //       _rb.velocity = new Vector2(_rb.velocity.y, _jumpForce);
           _rb.AddForce(_jumpDirection, ForceMode2D.Impulse);
        }
    }
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        GroundCheck();

        Debug.Log(_isGrounded);
    }
    private void FixedUpdate()
    {
        Movement();
    }
}
