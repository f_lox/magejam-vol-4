﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimOnClick : MonoBehaviour
{
    public Animator animator;
    public string ZoomInAnim = "Zoom In";
    public string ZoomOutAnim = "Zoom Out";
    public GameObject _michalGame;

    private void OnMouseDown()
    {
        animator.SetTrigger(ZoomInAnim);
        GetComponent<BoxCollider>().enabled = false;
        StartCoroutine("Cor");
        
    }

    public void ZoomOut()
    {
        animator.SetTrigger(ZoomOutAnim);
    }

    private IEnumerator Cor()
    {
        yield return new WaitForSeconds(1.3f);
        _michalGame.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
