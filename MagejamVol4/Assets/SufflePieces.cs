﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SufflePieces : MonoBehaviour
{

    public List<GameObject> pieces;

    private Bounds bounds;

    private void Start()
    {
        bounds = GetComponent<BoxCollider2D>().bounds;
        RandomizePiecesPositionInBounds();
    }

    private void RandomizePiecesPositionInBounds()
    {
        foreach (var piece in pieces)
        {
            piece.transform.localPosition = new Vector3(
                Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),
                piece.transform.localPosition.z
                );
        }
    }
}
