﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public List<PuzzleMove> puzzleMoves;
    public static PuzzleManager puzzleManager;
    private int puzzlePieces;
    [SerializeField] private GameObject _noise;

    private void Start()
    {
        puzzlePieces = puzzleMoves.Count;
        puzzleManager = this;
    }

    public void PuzzleCompletionCheck()
    {
        int piecesInPlace = 0;

        for (int i = 0; i < puzzlePieces; i++)
        {
            if (puzzleMoves[i].inSocket)
            {
                piecesInPlace++;
            }
        }

        if(piecesInPlace == puzzlePieces)
        {
            StartCoroutine(LastNoise());
        }
    }

    public IEnumerator LastNoise()
    {
        yield return new WaitForSeconds(5f);
        _noise.SetActive(true);
        yield return new WaitForSeconds(3f);
        Application.Quit();
    }
}
